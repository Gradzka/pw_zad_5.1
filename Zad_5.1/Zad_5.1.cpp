// Zad_5.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <windows.h> 
#include <time.h> 

#define random() (float)rand()/(float)RAND_MAX
#define nmax 10
typedef int t2[nmax][nmax];
typedef int t1[nmax];

t2 A;								//tablica reprezentująca graf
volatile int szerokosc = nmax - 1;
CRITICAL_SECTION CriticalSection;	// deklaracja sekcji krytycznej
void gnp(float p, t2 A) {
	int i, j;
	for (i = 0; i < nmax; i++) A[i][i] = 0; {
		for (i = 0; i < nmax - 1; i++){
			for (j = i + 1; j < nmax; j++) 
			{ 
				A[i][j] = A[j][i] = (random() <= p); 
				//printf("%f",random());
			}
		}
	}
}
void permInit(int nr_watku,t1 p) 
{ 
	int i;
	for (i = 0; i < nmax; i++)
	{
		if (nr_watku == 0)
			p[i] = i;
		else
		{
			if (i == 0) p[i] = nr_watku;
			else
			{
				if (i <=nr_watku)
				p[i] = i - 1;

				else p[i] = i;
			}
				
		}
	}
}
void druk(int L, t1 p) {
	int i;
		printf("%d: ", L);
		for (i = 0; i < nmax; i++) { 
			printf("%d ", p[i]);
		}
			printf("\n"); 
}
int permNext(int n,t1 p) {
	int i, j, x, b;
		b = 0;
		i = n - 1;
		while (i>0) {
			if (p[i]>p[i-1])
			{
				j = n - 1;
					while (p[j] < p[i-1])
						j--;
						x = p[j]; 
						p[j] = p[i-1]; 
						p[i-1] =x;
						while (i<n) {
							x = p[i]; 
							p[i] = p[n - 1];
							p[n - 1] = x;
								i++; 
								n--;
						}
						b = 1;
						break;
			}
			i--;
		}
	return b;
}
int naj_roznica(t2 A, t1 perm) {
	int dist, best = 1;
		int i, j;
		for (i = 0;i<nmax-1;i++)
			for (j = i + 1;j<nmax;j++) if (A[i][j])
			{
				dist = abs(perm[i]-perm[j]); 
				if (dist>best) best = dist;
			}
	return best;
}
void drukuj_tablice(t2 A)
{
	for (int i = 0; i < nmax; i++)
	{
		for (int j = 0; j < nmax; j++)
		{
			printf("%d ",A[i][j]);
		}
		printf("\n");
	}
}
int silnia(int n)
{
	if (n == 0 || n == 1) return 1;
	else return n*silnia(n - 1);
}
DWORD WINAPI ThreadProc_Szerokosc_Grafu(LPVOID lpParameter)
{

	t1 perm;		//wektor permutacji liczb 0-nmax
	int naj_r;
	int L = 1;
	int max_ilosc_perm_dla_watku = silnia(nmax - 1);

	permInit((int)lpParameter, perm);
	do {
		naj_r = naj_roznica(A, perm);

		EnterCriticalSection(&CriticalSection); //wejscie do sekcji krytycznej
		if (naj_r<szerokosc) szerokosc = naj_r;
		

		//printf("%d:	NR %d\n",(L+max_ilosc_perm_dla_watku*(int)lpParameter), naj_r);
		//druk(L, perm);
		
		if (szerokosc == 1|| L==max_ilosc_perm_dla_watku) 
		{
			break;
		}
		LeaveCriticalSection(&CriticalSection); //opuszczenie sekcji krytycznej
		
		L++;

	} while (permNext(nmax, perm));
	if (szerokosc == 1 || L == max_ilosc_perm_dla_watku)
	{
		LeaveCriticalSection(&CriticalSection); //opuszczenie sekcji krytycznej
	}
	return 0;
}
int _tmain(int argc, _TCHAR* argv[])
{
		srand(time(NULL));
		float p = 0.5f;
		gnp(p, A);
		drukuj_tablice(A);

		DWORD dwThreadId;
		HANDLE hThread[nmax];

		InitializeCriticalSection(&CriticalSection);
		volatile int k = 0;
		for (int i = 0; i<nmax; i++)
		{
			hThread[i] = CreateThread(NULL, 0, ThreadProc_Szerokosc_Grafu, (PVOID)k, 0, &dwThreadId);
			if (hThread[i] == NULL) ExitProcess(3); 
			k++;
		}
		for (int i = 0; i<nmax; i++)
		{
			WaitForSingleObject(hThread[i],INFINITE);
		}
		
		for (int i = 0; i<nmax; i++)
			CloseHandle(hThread[i]);

		DeleteCriticalSection(&CriticalSection);

		printf("Szerokosc grafu %d\n", szerokosc);
		system("pause");
		return 0;
}